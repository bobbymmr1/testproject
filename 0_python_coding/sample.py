import json
file_data = { line.split()[0] : [line.split()[1],line.split()[2]] for line in open("blkid.sample") }
result=[]
for key,each in file_data.items():
    try:
        result.append({'NAME':key})
        result.append({each[0].split('=')[0]:each[0].split('=')[1]})
        result.append({each[1].split('=')[0]:each[1].split('=')[1]})
    except Exception as e:
        pass
print(result)
