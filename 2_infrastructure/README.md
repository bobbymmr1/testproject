# IaC Challenge
Build out some basic Infrastructure for AWS, Azure or GCP using tooling like Ansible or Terraform that can be used in a repeatable way. 
Some ideas:

- Create a standalone non-production Docker host on EC2, optionally attaching an external EBS volume for persistent data.
- Provision an VM according to a user defined parameters (image, size, VM name, etc..)
- Get all running VMs and export them into a JSON or YAML file (as a dynamic inventory)
- Create a Lambda function, and configure it to be invoked on a schedule.
- Etc.. You may be more inspired!

Bonus points for the following:

* Clearly explaining why you're doing things a certain way.
* Providing a PNG diagram of your infrastructure.
